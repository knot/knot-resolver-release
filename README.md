knot-resolver-release
=====================

This repository contains files required to create `knot-resolver-release`
package. The purpose of this package is to set up a repository on the user's
system (similar to `epel-release`).

The `knot-resolver-release` package is used for the [official download
instructions](https://www.knot-resolver.cz/download/) for Knot Resolver on
Debian/Ubuntu distributions.

What does this package do?
--------------------------

- Installs the PGP signing key for Knot Resolver's OBS repositories
	- This key may change over time. If it does, it **needs** to be updated
	  here in time and the newly built package should be placed in
	  production OBS repos. This should ensure automatic update for all
	  users without the need for manual intervention to update the signing
	  key.
	- The signing key is automatically checked with a
	  [CI schedule](https://gitlab.nic.cz/knot/knot-resolver-release/-/pipeline_schedules).
- Installs `/etc/apt/sources.list.d/knot-resolver-latest.list`
	- The contents of this file is generated with
	  [postinst](debian/postinst) script. It automatically detects the
	  distro by reading `/etc/os-release` variables. If a new distro is to
	  be supported, it needs to be added to this script, and the package
	  has to be rebuilt/re-released.

Why does it exist?
------------------

1. To update people's OBS signing key without manual intervention.
2. To simplify repository set-up ([OBS installation
   instructions](https://software.opensuse.org//download.html?project=home%3ACZ-NIC%3Aknot-resolver-latest&package=knot-resolver)
   are a nightmare).
3. To allow for possible future seemless migration to completely different repositories.
	- Change the signing key and repository URL.
	- Release the package as version 2.x
	- Place it into OBS repo
	- When users do distro update, their system will be set-up with the new repos

Supported distros
-----------------

Currently, only *debian-based distros are supported*, since there are no
official [upstream Knot Resolver
repositories](https://www.knot-resolver.cz/download/) for Fedora/EL and
Fedora/EPEL repositories are used directly.

Fedora/EPEL package was available in the past, but it has been discontinued
because upstream repos for Fedora/EL are no longer supported (but they are
still available in OBS so far).

Releasing the package
---------------------

Script `./build-in-obs.sh` can be used to rebuild and release the package.

- Bump the version in the script
- Build it in `knot-resolver-testing` repo first and check the package works as
  expected (especially if adding new distro - e.g. in a VM/container)
- If everything works as expected, re-run the script to release the package
  into `knot-resolver-latest` repo
- Once built, download the package, rename it to `knot-resolver-release.deb`
  and publish it at https://secure.nic.cz/files/knot-resolver/ (ask the person
  doing Knot Resolver releases to sign the package and upload it here)
- Perform a final check that the uploaded package works as expected using
  `tests/test-deb.sh`

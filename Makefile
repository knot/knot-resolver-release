all: cznic-obs.gpg

cznic-obs.gpg:
	gpg --dearmor cznic-obs.gpg.asc
	mv cznic-obs.gpg.asc.gpg cznic-obs.gpg

clean:
	rm -f cznic-obs.gpg

.PHONY: clean

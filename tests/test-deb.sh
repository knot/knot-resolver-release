#!/usr/bin/bash
# Usage:
# ./test-deb.sh [URL] [docker_image]
# example: ./test-deb.sh https://secure.nic.cz/files/knot-resolver/knot-resolver-release.deb debian:10

set -o errexit

URL=${1:-https://secure.nic.cz/files/knot-resolver/knot-resolver-release.deb}
IMAGE=${2:-debian:10}

SCRIPT="export DEBIAN_FRONTEND=noninteractive && \
apt-get update && \
apt-get install -y wget && \
wget -O knot-resolver-release.deb $URL && \
dpkg -i knot-resolver-release.deb && \
apt update && \
apt install -y knot-resolver && \
dpkg -s knot-resolver | grep '^Version'"

docker rm -f tmp-knot-resolver-release &>/dev/null ||:
docker run -itd --name=tmp-knot-resolver-release $IMAGE /bin/bash
docker exec -it tmp-knot-resolver-release /bin/bash -c "$SCRIPT"
docker rm -f tmp-knot-resolver-release &>/dev/null ||:

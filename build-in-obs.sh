#!/bin/bash
set -o errexit -o nounset -o xtrace

# Example usage:
# ./build-in-obs.sh knot-resolver-latest

project=home:CZ-NIC:$1
package=knot-resolver-release
version=1.11

if ! [[ "$1" == *-devel || "$1" == *-testing ]]; then
	read -p "Pushing to '$project', are you sure? [y/N]: " yn
	case $yn in
		[Yy]* )
            ;;
		* )
            exit 1
	esac
fi

# create "upstream" tarball
pkgdir=$package-$version
rm -rf $pkgdir
mkdir $pkgdir
cp -t $pkgdir cznic-obs.gpg.asc Makefile LICENSE
tar czf ${package}_${version}.orig.tar.gz $pkgdir

# create debian archive and dsc
pushd "${pkgdir}" > /dev/null
cp -arL ../debian debian
dpkg-source -b .
popd > /dev/null

rm -rf $pkgdir

# push to OBS
osc co "${project}" "${package}"
pushd "${project}/${package}"
osc del * ||:
cp -L ../../*.orig.tar.gz ../../*.debian.tar.xz ../../*.dsc ./
osc addremove
osc ci -n
popd
